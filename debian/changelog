libhmsbeagle (4.0.1+dfsg-3) unstable; urgency=medium

  * Team upload
  * Disabling the build of SSE parts as it fails on most arches
  * Skipping synthetictest as it requires SSE symbols on some arches

 -- Pierre Gruet <pgt@debian.org>  Mon, 16 Dec 2024 15:14:00 +0100

libhmsbeagle (4.0.1+dfsg-2) unstable; urgency=medium

  * Team upload
  * Modifying d/docs to ship the files in libhmsbeagle-dev

 -- Pierre Gruet <pgt@debian.org>  Mon, 16 Dec 2024 10:35:35 +0100

libhmsbeagle (4.0.1+dfsg-1) unstable; urgency=medium

  * Team upload
  * Building the jar file
  * Installing all the .so files in the -dev package
  * New upstream version 4.0.1+dfsg
  * Raising Standards version to 4.7.0 (no change)
  * Making it explicit that we won't ship -opencl shared lib, as has been the
    case up to now
  * Documentation files are not built anymore, removing them from doc-base

  [ Andreas Tille ]
  * New upstream version 4.0.0+dfsg
  * Refresh patches
  * Upstream switched build system to cmake
  * d/watch: Create proper upstream filename

 -- Pierre Gruet <pgt@debian.org>  Sun, 15 Dec 2024 22:11:23 +0100

libhmsbeagle (3.1.2+dfsg-13) unstable; urgency=medium

  * Team upload
  * Stopping supporting OpenCL (Closes: #1025953)

 -- Pierre Gruet <pgt@debian.org>  Fri, 16 Dec 2022 22:41:31 +0100

libhmsbeagle (3.1.2+dfsg-12) unstable; urgency=medium

  * Drop beignet-opencl-icd from explicit Depends
    Closes: #996618
  * Remove Tim Booth from Uploaders (Tim, thank you for all your previous
    work)

 -- Andreas Tille <tille@debian.org>  Mon, 18 Oct 2021 09:42:01 +0200

libhmsbeagle (3.1.2+dfsg-11) unstable; urgency=medium

  * Fix watchfile to detect new versions on github (routine-update)
  * Standards-Version: 4.6.0 (routine-update)
  * Apply multi-arch hints.
    + libhmsbeagle-java, libhmsbeagle1v5: Add Multi-Arch: same.

 -- Andreas Tille <tille@debian.org>  Sun, 22 Aug 2021 10:58:21 +0200

libhmsbeagle (3.1.2+dfsg-10) unstable; urgency=medium

  * Do not confuse DEB_HOST_MULTIARCH and DEB_HOST_GNU_TYPE
    Closes: #950367

 -- Andreas Tille <tille@debian.org>  Wed, 23 Dec 2020 12:05:09 +0100

libhmsbeagle (3.1.2+dfsg-9) unstable; urgency=medium

  * Standards-Version: 4.5.1 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Set upstream metadata fields: Repository, Repository-Browse.
  * Install all *.a files and remove *.la files from debian/tmp
  * Add missing stdlib.h
    Closes: #976567

 -- Andreas Tille <tille@debian.org>  Sun, 06 Dec 2020 16:53:18 +0100

libhmsbeagle (3.1.2+dfsg-8) unstable; urgency=medium

  * libhmsbeagle1v5: Architecture: linux-any
    Closes: #960260
  * Standards-Version: 4.5.0 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)

 -- Andreas Tille <tille@debian.org>  Mon, 11 May 2020 11:46:51 +0200

libhmsbeagle (3.1.2+dfsg-7) unstable; urgency=medium

  [ Frédéric Bonnard ]
  * Fix ppc64el FTBFS with altivec (Closes: #922776)

  [ Andreas Tille ]
  * debhelper-compat 12
  * Standards-Version: 4.4.1
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Remove obsolete field Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).

 -- Andreas Tille <tille@debian.org>  Sun, 12 Jan 2020 19:08:31 +0100

libhmsbeagle (3.1.2+dfsg-6) unstable; urgency=medium

  * Team upload.
  * Replace DEB_HOST_GNU_TYPE by DEB_HOST_MULTIARCH
    Closes: #929560

 -- Dylan Aïssi <daissi@debian.org>  Sun, 26 May 2019 22:06:37 +0200

libhmsbeagle (3.1.2+dfsg-5) unstable; urgency=medium

  * Re-add libhmsbeagle1v5.dirs

 -- Andreas Tille <tille@debian.org>  Fri, 22 Feb 2019 15:31:02 +0100

libhmsbeagle (3.1.2+dfsg-4) unstable; urgency=medium

  * Provide libhmsbeagle-jni.so as symlink in jni subdir
    Closes: #922778

 -- Andreas Tille <tille@debian.org>  Fri, 22 Feb 2019 13:51:26 +0100

libhmsbeagle (3.1.2+dfsg-3) unstable; urgency=medium

  * Fix dankling symlink
    Closes: #919582
  * debhelper 12
  * Standards-Version: 4.3.0

 -- Andreas Tille <tille@debian.org>  Thu, 17 Jan 2019 18:36:10 +0100

libhmsbeagle (3.1.2+dfsg-2) unstable; urgency=medium

  * Fix broken symlink
    Closes: #858524

 -- Andreas Tille <tille@debian.org>  Sat, 22 Dec 2018 07:21:35 +0100

libhmsbeagle (3.1.2+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Force LC_ALL=C.UTF-8 encoding
  * Standards-Version: 4.2.1
  * Secure URI in copyright format
  * Respect DEB_BUILD_OPTIONS in override_dh_auto_test target
  * Remove trailing whitespace in debian/changelog
  * Remove trailing whitespace in debian/rules
  * Ignore test failures which are most probably not relevant on real
    hardware but occur in restricted Debian build environment (see
    https://github.com/beagle-dev/beagle-lib/issues/121 )

 -- Andreas Tille <tille@debian.org>  Tue, 18 Dec 2018 11:28:44 +0100

libhmsbeagle (2.1.2+git20180307-1) unstable; urgency=medium

  * New Github commit
  * d/watch now scans Github commits (deleted get-orig-source)
  * Standards-Version: 4.1.4
  * Point Vcs fields to salsa.debian.org
  * Remove unused lintian override

 -- Andreas Tille <tille@debian.org>  Wed, 11 Apr 2018 14:02:53 +0200

libhmsbeagle (2.1.2+20171110-1) unstable; urgency=medium

  * New upstream version
  * debhelper 11
  * Standards-Version: 4.1.3 (no changes needed)
  * Drop Build-Depends: libpoclu-dev
    Closes: #891172
  * hardening=+all

 -- Andreas Tille <tille@debian.org>  Fri, 23 Feb 2018 09:08:56 +0100

libhmsbeagle (2.1.2+20160831-5) unstable; urgency=medium

  * Restrict Architecture to those where beignet-opencl-icd|mesa-opencl-icd
    is available (mesa-opencl-icd exists for 4 architectures)
    Closes: #842639

 -- Andreas Tille <tille@debian.org>  Wed, 02 Nov 2016 15:12:30 +0100

libhmsbeagle (2.1.2+20160831-4) unstable; urgency=medium

  * Build-Depends: pocl-opencl-icd [amd64]

 -- Andreas Tille <tille@debian.org>  Mon, 17 Oct 2016 06:51:10 +0200

libhmsbeagle (2.1.2+20160831-3) unstable; urgency=medium

  * Build-Depends: libpoclu-dev [amd64]

 -- Andreas Tille <tille@debian.org>  Sun, 16 Oct 2016 20:00:42 +0200

libhmsbeagle (2.1.2+20160831-2) unstable; urgency=medium

  * Skip tests on other architectures than amd64.
    Closes: #840725

 -- Andreas Tille <tille@debian.org>  Sun, 16 Oct 2016 15:08:44 +0200

libhmsbeagle (2.1.2+20160831-1) unstable; urgency=medium

  * New upstream commit
  * d/watch pointing to Github
  * ocl-icd-opencl-dev now provided by khronos-opencl-clhpp
    Closes: #840725
  * Fixed symlink
    Closes: #827120

 -- Andreas Tille <tille@debian.org>  Sat, 15 Oct 2016 13:15:32 +0200

libhmsbeagle (2.1.2+20160525-1) unstable; urgency=medium

  * New git commit from upstream repository
  * Fix version detection in debian/get-orig-source
  * cme fix dpkg-control
  * Provide valid HOME dir before starting test suite
    Closes: #831235
  * hardening=+bindnow

 -- Andreas Tille <tille@debian.org>  Thu, 14 Jul 2016 16:49:10 +0200

libhmsbeagle (2.1.2+20151220-1) unstable; urgency=medium

  * New upstream Git checkout
  * debian/get-orig-source: Set date stamped version automatically
  * Build using OpenCL
  * Update URLs from GoogleCode to GitHub
  * Add missing libhmsbeagle1v5 dependency to -java package
  * Delete outdated information from debian/upstream/metadata
  * Add doc-base file

 -- Andreas Tille <tille@debian.org>  Thu, 28 Jan 2016 08:56:11 +0100

libhmsbeagle (2.1.2+20150609-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Rename library packages for g++5 ABI transition. (Closes: #791128)

 -- Martin Pitt <mpitt@debian.org>  Wed, 05 Aug 2015 11:44:16 +0200

libhmsbeagle (2.1.2+20150609-1) unstable; urgency=medium

  * New Git checkout featuring functionality used in beast-mcmc release
  * Moved from SVN to Git
  * Updated debian/copyright

 -- Andreas Tille <tille@debian.org>  Sat, 18 Jul 2015 20:35:03 +0200

libhmsbeagle (2.1.2-1) unstable; urgency=medium

  * New upstream version
  * Moved debian/upstream to debian/upstream/metadata
  * cme fix dpkg-control

 -- Andreas Tille <tille@debian.org>  Wed, 01 Oct 2014 11:36:11 +0200

libhmsbeagle (2.1-2) unstable; urgency=medium

  * debian/libhmsbeagle-dev.links: Link to non-versioned include files since
    it is used that way in mrbayes (and probably others)

 -- Andreas Tille <tille@debian.org>  Mon, 13 Jan 2014 17:00:56 +0100

libhmsbeagle (2.1-1) unstable; urgency=medium

  * New upstream version
    manpages for examples are not built by default
    Closes: #713882
  * debian/control:
     - Standards-Version: 3.9.5
     - Build-Depends: graphviz

 -- Andreas Tille <tille@debian.org>  Sun, 22 Dec 2013 21:45:23 +0100

libhmsbeagle (1.1r1092-2) unstable; urgency=low

  * Upload to unstable because it
    Closes: #720562
  * debian/control:
     - cme fix dpkg-control
     - anonscm in Vcs fields

 -- Andreas Tille <tille@debian.org>  Fri, 23 Aug 2013 14:17:18 +0200

libhmsbeagle (1.1r1092-1) experimental; urgency=low

  * Package SVN trunk because it is required for beast-mcmc

 -- Andreas Tille <tille@debian.org>  Sun, 16 Dec 2012 21:47:26 +0100

libhmsbeagle (1.1-1) experimental; urgency=low

  * New upstream release
     - debian/patches/disable_cpu_sse_plugin.patch:  Seems upstream
       has solved the problem a bit differently than in this patch
       -> droping it for the moment
  * debian/get-orig-source: use xz compression
  * debhelper 9 (control+compat)
  * debian/libhmsbeagle-dev.lintian-overrides: deleted; no need for overrides
    any more
  * debian/control:
     - Build-Depends: s/openjdk-6-jdk/default-jdk (>= 1:1.6)/
       (Thanks for the patch to James Page <james.page@ubuntu.com>)
       Closes: #684054
     - Pre-Depend: multiarch-support

 -- Andreas Tille <tille@debian.org>  Fri, 14 Dec 2012 19:23:52 +0100

libhmsbeagle (1.0-6) unstable; urgency=low

  * debian/upstream:
     - Make authors BibTeX compliant
     - Add some publication info (volume, number, pages)
  * debian/get-orig-source: Try harder to get reproducible tarballs
  * debian/patches/gcc-4.7.patch: Fix gcc 4.7 build issue
    Closes: #672015

 -- Andreas Tille <tille@debian.org>  Wed, 09 May 2012 15:08:00 +0200

libhmsbeagle (1.0-5) unstable; urgency=low

  [Charles Plessy]
  * debian/upstream
    - renamed from debian/upstream-metadata.yaml
    - fixed title field

  [Andreas Tille]
  * Remove useless remainings which were forgotten to clean up in last
    upload (debian/rules_peter; template comments in
    debian/patches/disable_cpu_sse_plugin.patch
  * debian/copyright:
    - Aaron Darling as Upstream-Contact because he seems to be competent for
      packaging issues
    - run `cme fix dpkg-copyright' to check for DEP5 issues
  * debian/control:
    - Architecture: linux-any because openjdk as is not available for kfreebsd
    - Build-Depends: s/default-jdk/openjdk-6-jdk/
      Closes: #661659
    - Standards-Version: 3.9.3 (no changes needed)
  * debhelper 8 (control+compat)
  * debian/upstream: moved DOI+PMID to References

 -- Andreas Tille <tille@debian.org>  Mon, 05 Mar 2012 10:06:23 +0100

libhmsbeagle (1.0-4) unstable; urgency=low

  * debian/patches/disable_cpu_sse_plugin.patch: Apply patch from Peter
    Green (thanks for your help, Peter) and
    debian/rules: Enable sse for amd64 (only) to profit from plugin library
    Closes: #656755

 -- Andreas Tille <tille@debian.org>  Wed, 25 Jan 2012 13:54:59 +0100

libhmsbeagle (1.0-3) unstable; urgency=low

  * debian/patches/enable_static.patch: Enable building static
    libraries that should be contained in -dev package
  * debian/libhmsbeagle-dev.install: Install static libraries

 -- Andreas Tille <tille@debian.org>  Sat, 21 Jan 2012 17:27:53 +0100

libhmsbeagle (1.0-2) unstable; urgency=low

  * debian/rules: Prevent compiler options -march=native and -msse2
    Closes: #656729

 -- Andreas Tille <tille@debian.org>  Sat, 21 Jan 2012 13:54:38 +0100

libhmsbeagle (1.0-1) unstable; urgency=low

  * Initial upload to Debian (Closes: #656609)
  * debian/get-orig-source: Fetch tagged version instead of trunk
  * debian/watch: Detect new tags
  * debian/control:
    - Add Java library package
    - Added needed Java (Build-)Depends
    - Moved citation information to upstream-metadata.yaml and
      README.Debian
  * debian/rules: Build Java Library
  * debian/copyright: DEP5
  * debian/libhmsbeagle-dev.lintian-overrides: Make less noise about
    broken doxygen manpages
  * debian/upstream-metadata.yaml: Citation information

 -- Andreas Tille <tille@debian.org>  Thu, 19 Jan 2012 22:43:44 +0100

libhmsbeagle (0.1052-2) unstable; urgency=low

  * Initial build for PPA/experimental.

 -- Tim Booth <tbooth@ceh.ac.uk>  Mon, 12 Dec 2011 17:52:41 +0000
